\chapter{Алгоритмы решения ориентированной задачи Штейнера} \label{ch2}
	
Данная глава посвящена описанию алгоритмов решения ориентированной задачи Штейнера и программной реализации всех алгоритмов. В параграфе \ref{ch2:secDW} описан алгоритм, основанный на алгоритме Дрейфуса, Вагнера. В параграфе \ref{ch2:secEMV} описан алгоритм, основанный на алгоритме Эриксона, Монмы, Вейнотта.

Идеи данных алгоритмов предложил Пастор Алексей Владимирович.

\section{Алгоритм решения ориентированной задачи Штейнера на базе алгоритма ДВ} \label{ch2:secDW}
\subsection{Описание алгоритма нахождения стоимости}
Основная идея алгоритма остаётся той же самой, однако функции $f_v$ и $g_v$ несколько изменяют свою семантику. $f_v(X)$ теперь имеет смысл стоимости дерева Штейнера для задачи с корнем $v$ и множеством терминалов $X$. $g_v(X)$ приобретает смысл верхней оценки для стоимости дерева Штейнера для задачи с корнем $v$ и множеством терминалов $X$. Оценка вырождается в точную стоимость, когда степень вершины $v$ в оптимальном решении не меньше двух.

Стоит также отметить, что в ориентированном случае не все вершины могут быть достижимы друг из друга, в связи с чем алгоритм Дейкстры может возвращать бесконечные расстояния. Тогда в переменной, содержащей сумму бесконечного расстояния и значения $f_u$  или $g_u$ окажется заведомо неверное число. Таким образом, для исключения неправильного вычисления $f_v$ необходимо отдельно отслеживать появление бесконечного расстояния, и отказываться от рассмотрения таких сумм.

\subsection{Доказательство корректности алгоритма нахождения стоимости} \label{ch2:DW:DWcorr}
\begin{m-lemma}[о функции $g_v$] \label{ch2:DW:lemmagv}
	Функция $g_v$, вычисляемая по формуле \ref{eq:ch1:DW:gv}, является оценкой сверху для стоимости дерева Штейнера с корнем $v$, построенного для множества терминалов $X$, которая оказывается точной в том случае, когда степень вершины $v$ в дереве Штейнера не меньше двух.
\end{m-lemma}

\textit{Доказательство}

Пусть $T$ -- дерево Штейнера для множества терминалов $X$ и корня $v$, причём $\deg_T v \geq 2$. Тогда существует такое разбиение $X$ на два непустых подмножества  $X'$ и $X \backslash X'$, что дерево $T$ можно разрезать на два поддерева по вершине $v$, и множество покрытых терминалов у них окажется соответственно $X'$ и $X \backslash X'$. Назовём получившиеся деревья $T_{v, X'}$ и $T_{v, X \backslash X'}$. Для них выполняется: 
\begin{equation} \label{eq:ch2:gvl:T}
cost(T)=cost(T_{v, X'}) + cost(T_{v, X \backslash X'})
\end{equation}

Пусть $T'_{v, X'}, T'_{v, X \backslash X'}$ -- деревья Штейнера для корня $v$ и множества терминалов $X', X \backslash X'$ соответственно. Построим дерево $T'$ как объединение деревьев $T'_{v, X'}$ и $T'_{v, X \backslash X'}$.

Покажем, что $cost(T'_{v, X'})=cost(T_{v, X'})$ и $cost(T'_{v, X \backslash X'}) = cost(T_{v, X \backslash X'})$. Будем действовать от противного. Предположим, что $cost(T'_{v, X'}) < cost(T_{v, X'})$. \footnote{Доказательство равенства $cost(T'_{v, X \backslash X'})$ и $cost(T_{v, X \backslash X'})$ производится аналогично.} Но тогда:
\begin{equation}
cost(T') = cost(T'_{v, X'}) + cost(T'_{v, X \backslash X'}) < cost(T_{v, X'}) + cost(T_{v, X \backslash X'}) \overset{\ref{eq:ch2:gvl:T}}{=} cost(T)
\end{equation}
Получили, что дерево $T'$ с корнем $v$, и листовым множеством $X$, обладает меньшей стоимостью, чем дерево Штейнера $T$. Противоречие.

Следовательно, $cost(T'_{v, X'}) = cost(T_{v, X'})$ и $cost(T'_{v, X \backslash X'}) = cost(T_{v, X \backslash X'})$, а, значит, $g_v(X) = f_v(X') + f_v(X \backslash X')$.

Если же $\deg_T v = 1$, то оба дерева $T_{v, X'}$ и $T_{v, X \backslash X'}$ содержат ребро $e$, инцидентное $v$, а, значит, $cost(T) > cost(T_{X'}) + cost(T_{X \backslash X'})$, поскольку стоимость ребра $e$ учитывается дважды. \begin{flushright}$\square$\end{flushright}

\begin{m-lemma}[о функции $f_v$] \label{ch2:DW:lemmafv}
	Функция $f_v$, вычисляемая по формуле \ref{eq:ch1:DW:fv}, позволяет найти стоимость дерева Штейнера с корнем $v$, построенного для терминалов $X$.
\end{m-lemma}

\textit{Доказательство}

Если $|X| = 1$, то стоимость дерева Штейнера совпадает с длиной кратчайшего пути между $v$ и $X_0$ (единственного элемента $X$), что соответствует \ref{eq:ch1:DW:fv}.

Если $\deg_T v \geq 2$, то $f_v(X)=g_v(X)$ при замене $u=v$ в \ref{eq:ch1:DW:gv}.

Осталось показать корректность формулы для случая, когда степень корня равна 1. Обозначим за $P$ путь от $v$ до некоторой вершины $u$ в дереве $T$.
Рассмотрим два случая:
\begin{enumerate}[1.]
	\item $u \in X$
	\item $u \in V \backslash X$
\end{enumerate}

\textbf{Случай 1.} Разрежем $T$ на поддерево c корнем $u$ и множеством терминалов $X$ и путь $P$ от $v$ до $u$.
Используем обозначения, аналогичные введённым в \ref{ch2:DW:lemmagv}.

Покажем, что $cost(T_{v, X}) = cost(T'_{v, X})$, а $P$ -- кратчайший путь от $v$ до $u$.

Пусть $cost(T_{v, X}) > cost(T'_{v, X})$. Но тогда $cost(T') = cost(T'_{v, X}) + cost(P) < cost(T)$, то есть дерево $T'$ обладает стоимостью, меньшей, чем дерево Штейнера. Получили противоречие.

Аналогично показывается, что $P$ -- кратчайший путь от $v$ до $u$.

Таким образом, $f_v(X) = d(v, u) + f_u(X \backslash \{ u\})$ при $u \in X$.

\textbf{Случай 2.} Идея доказательства случая 2 полностью аналогична случаю 1: дерево $T$ разделяется на путь от $v$ до $u$ и оставшуюся часть $T_{v, X \backslash \{ v \}}$. Затем делается предположение, что дерево Штейнера $T'_{v, X \backslash \{ v \}}$ имеет меньшую стоимость, чем $T_{v, X \backslash \{ v \}}$. Строится новое дерево $T'$ как объединение $T'_{v, X \backslash \{ v \}}$ и $P$, которое получает стоимость, меньшую, чем дерево Штейнера $T$, что противоречит условию.

Таким образом, $f_v(X) = d(v, u) + g_u(X)$ при $u \in V \backslash X$. \begin{flushright}$\square$\end{flushright}
	
\section{Алгоритм решения ориентированной задачи Штейнера на базе алгоритма ЭМВ} \label{ch2:secEMV}
\subsection{Описание алгоритма нахождения стоимости} \label{ch2:EMV:descr}
В этом алгоритме $f_v$ и $g_v$ приобретают тот же смысл, что и в случае модернизации алгоритма Дрейфуса, Вагнера для решения ориентированной задачи.

Поскольку задача ориентированная, расстояние между некоторыми вершинами $v, u$ не будет равно расстоянию между вершинами $u, v$. Поэтому при инициализации начальных значений $f_v(u)$ необходимо вызывать алгоритм Дейкстры именно из вершины $v$, а не $u$. Однако в таком случае потребовалось бы совершить $n$ вызовов алгоритма Дейкстры, что приведёт к значительному снижению производительности алгоритма.

Решением этого вопроса является следующая последовательность действий: необходимо создать граф, в котором все рёбра будут иметь те же веса, что и в исходном (назовём его \textbf{инвертированным графом}), но будут направлены в противоположную сторону, далее вызывать алгоритм Дейкстры для всех терминалов. Чтобы получить расстояния, достаточно просто обратиться к соответствующему массиву, а чтобы получить верный путь между $u$ и $v$ в исходном графе, все рёбра пути в инвертированном графе от $v$ до $u$ необходимо развернуть. Таким образом, по-прежнему будет совершаться только $k$ вызовов алгоритма Дейкстры и будет выполняться однократная процедура инвертирования графа.

Такая же проблема возникает при дальнейшем вычислении $f_v$. В неориентированной версии алгоритма ЭМВ вычисление $f_v(X)$ происходит путём сложения стоимости вспомогательного ребра $f_u(X \backslash \{u\})$ либо $g_u(X)$  (см.\ref{eq:ch1:DW:fv}) и расстояния $d(u, v)$ между вершинами $u$ и $v$. В неориентированной версии $d(u, v) = d(v, u)$, поэтому нарушения полученной формулы не было, однако в ориентированном случае, как было ранее сказано, эти расстояния принимают разные значения. Поэтому в ориентированной версии алгоритма ЭМВ необходимо строить вспомогательный граф не над оригинальным графом, а над инвертированным. В таком случае, если в графе $G$ $u$ достижима из $v$, то в инвертированном $v$ будет достижима из $u$, а расстояние сохранится тем же. Таким образом, стоимость будет рассчитана корректно. О том, как эти детали учитываются на уровне кода, более подробно можно прочитать в параграфе \ref{ch3:progEMV}.

\subsection{Доказательство корректности алгоритма нахождения стоимости} \label{ch2:EMV:EMVcorr}

Коррректность формул \ref{eq:ch1:DW:fv} и \ref{eq:ch1:DW:gv} была показана в разделе \ref{ch2:DW:DWcorr}, посвящённом доказательству корректности алгоритма решения ориентированной задачи на базе алгоритма ДВ.

Отличие данного алгоритма от алгоритма на базе алгоритма ДВ состоит в \textit{реализации} формулы $f_v$.
Покажем, что приведённый в \ref{ch2:EMV:descr} метод является корректным.

Стоимость кратчайшего пути от $r$ до некоторой вершины $v$ в графе $G'$ равняется:

\begin{equation}
d_{G'}(r, v) = \underset{u \in V} {\min}
\begin{cases}
f_u(X \backslash \{u\}) + d_{invG}(u, v) = f_u(X \backslash \{u\}) + d_G(v, u), & u \in X \\
g_u(X) + d_{invG}(u, v) = g_u(X) + d_G(v, u), & u \in V \backslash X
\end{cases}
\end{equation}

Таким образом, $d_{G'}(r, v) = f_v(X)$.
\begin{flushright}
	$\square$
\end{flushright}

%% Вспомогательные команды - Additional commands
%
%\newpage % принудительное начало с новой страницы, использовать только в конце раздела
%\clearpage % осуществляется пакетом <<placeins>> в пределах секций
%\newpage\leavevmode\thispagestyle{empty}\newpage % 100 % начало новой страницы